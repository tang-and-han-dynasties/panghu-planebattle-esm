//初始化奖励类
class Award {
    constructor(config) {
        this.type = config.type;
        this.width = config.width;
        this.height = config.height;
        this.x = Math.floor(Math.random() * (480 - config.width));
        this.y = -config.height;
        this.life = config.life;
        this.score = config.score;
        this.img = config.img;
        this.live = true;
        this.speed = Math.floor(Math.random() * (config.minSpeed - config.maxSpeed + 1)) + config.maxSpeed;
        this.lastTime = new Date().getTime();
        this.deathIndex = 0;
        this.destory = false;
    }
    move() {
        const currentTime = new Date().getTime();
        if (currentTime - this.lastTime >= this.speed) {
            if (this.live) {
                this.y = this.y + 6;
                this.lastTime = currentTime;
            } else {
                this.destory = true;

            }
        }
    }
    paint(context) {
        context.drawImage(this.img, this.x, this.y, this.width, this.height);
    }
    outOfBounds() {
        if (this.y > 650) {
            return true;
        }
    }
    hit(o) {
        let ol = o.x;
        let or = o.x + o.width;
        let ot = o.y;
        let ob = o.y + o.height;
        let el = this.x;
        let er = this.x + this.width;
        let et = this.y;
        let eb = this.y + this.height;
        if (ol > er || or < el || ot > eb || ob < et) {
            return false;
        } else {
            return true;
        }
    }
    // collide() {
    //   this.life--;
    //   if (this.life === 0) {
    //     this.live = false;
    //     score += this.score;
    //   }
    // }
}

export default Award 