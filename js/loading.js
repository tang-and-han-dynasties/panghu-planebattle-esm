// 初始化一个飞机界面加载类
import { RUNNING } from "./config";
class Loading {
  constructor(config) {

    this.frame = config.frame;
    this.img = this.frame;
    this.frameIndex = 0;
    this.width = config.width;
    this.height = config.height;
    this.x = config.x;
    this.y = config.y;
    this.speed = config.speed;
    this.lastTime = new Date().getTime();
  }
  judge() {
    const currentTime = new Date().getTime();
    if (currentTime - this.lastTime > this.speed) {
      this.frameIndex++;
      if (this.frameIndex === 4) {
        state = RUNNING;
      }
      this.lastTime = currentTime;
    }
  }
  paint(context) {
    if (this.frameIndex < 3) {
      context.drawImage(this.img[this.frameIndex], this.x, this.y);
    }
  }
}

export default Loading