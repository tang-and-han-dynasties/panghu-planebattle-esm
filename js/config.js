//图片初始化方法
function createImage(src) {
    let img;
    if (typeof src === "string") {
        img = new Image();
        img.src = require('../img/' + src);
    } else {
        img = [];
        for (let i = 0; i < src.length; i++) {
            img[i] = new Image();
            img[i].src = require('../img/' + src[i]);
        }
    }
    return img;
}

const IMAGES = {
    b: "bullet1.png",
    bg: "4.png",
    copyright: "shoot_copyright.png",
    pause: "game_pause.png",
    loading_frame: ["game_loading1.png", "game_loading2.png", "game_loading3.png",
        "game_loading4.png"
    ],
    hero_frame_live: ["hero1.png", "hero2.png"],
    hero_frame_death: ["hero_blowup_n1.png", "hero_blowup_n2.png", "hero_blowup_n3.png",
        "hero_blowup_n4.png"
    ],
    e1_live: ["enemy1.png"],
    e1_death: ["enemy1_down1.png", "enemy1_down2.png", "enemy1_down3.png", "enemy1_down4.png"],
    e2_live: ["enemy2.png"],
    e2_death: ["enemy2_down1.png", "enemy2_down2.png", "enemy2_down3.png", "enemy2_down4.png"],
    e3_live: ["enemy3_n1.png", "enemy3_n2.png"],
    e3_death: ["enemy3_down1.png", "enemy3_down2.png", "enemy3_down3.png", "enemy3_down4.png",
        "enemy3_down5.png", "enemy3_down6.png"
    ],
    c1: "lanqiu.png"
};
//初始化各个图片
const b = createImage(IMAGES.b);
export const bg = createImage(IMAGES.bg);
export const copyright = createImage(IMAGES.copyright);
export const pause = createImage(IMAGES.pause);
const loading_frame = createImage(IMAGES.loading_frame);
const hero_frame = {
    live: createImage(IMAGES.hero_frame_live),
    death: createImage(IMAGES.hero_frame_death),
};
const e1 = {
    live: createImage(IMAGES.e1_live),
    death: createImage(IMAGES.e1_death),
};
const e2 = {
    live: createImage(IMAGES.e2_live),
    death: createImage(IMAGES.e2_death),
};
const e3 = {
    live: createImage(IMAGES.e3_live),
    death: createImage(IMAGES.e3_death),
};
const c1 = createImage(IMAGES.c1);


//天空类的配置项
export const SKY = {
    bg: bg,
    width: 480,
    height: 650,
    speed: 10,
};

// 飞机加载界面的配置项
export const LOADING = {
    frame: loading_frame,
    width: 186,
    height: 38,
    x: 0,
    y: 650 - 38,
    speed: 400,
};

// 英雄配置项
export const HERO = {
    frame: hero_frame,
    width: 99,
    height: 124,
    speed: 100,
};

// 子弹配置项
export const BULLET = {
    img: b,
    width: 9,
    height: 21,
};

//小敌机配置项
export const E1 = {
    type: 1,
    width: 57,
    height: 51,
    life: 10,
    score: 1,
    frame: e1,
    minSpeed: 20,
    maxSpeed: 10
};
//中敌机配置项
export const E2 = {
    type: 2,
    width: 69,
    height: 95,
    life: 50,
    score: 5,
    frame: e2,
    minSpeed: 50,
    maxSpeed: 20
};
//打敌机配置项
export const E3 = {
    type: 3,
    width: 169,
    height: 258,
    life: 100,
    score: 20,
    frame: e3,
    minSpeed: 100,
    maxSpeed: 100
};
//奖励类配置项
export const C1 = {
    type: 4,
    width: 75,
    height: 75,
    life: 1,
    score: 1,
    img: c1,
    minSpeed: 5,
    maxSpeed: 10
};


//配置项:
// 定义游戏的状态
// 开始
export const START = 0;
// 开始时
export const STARTING = 1;
// 运行时
export const RUNNING = 2;
// 暂停时
export const PAUSE = 3;
// 结束时
export const END = 4;
// 加载中
export const LOADINGING = 5;

window.state = 0;

//score 分数变量 life 变量
window.score = 0;
window.life = 3;

export let canvas = document.createElement('canvas');
// this.$refs.stage.appendChild(canvas);
canvas.width = 480;
canvas.height = 650;
canvas.ref = canvas;
canvas.style = "border: 1px solid red;"
export let context = canvas.getContext("2d");