## 这是panghu-planebattle-esm的.md说明文档

欢迎使用胖虎的飞机大战包!!
为你的主页添加色彩

- 这是一个有趣的网页小游戏包,使用canvas和js开发
- 使用ES6模块化开发

<br>

## 使用我
## 1.导入

### 1.1.在vue中使用

第一步:使用npm导入包:
```
npm install panghu-planebattle-esm
```

或使用在gitee中下载
https://gitee.com/tang-and-han-dynasties/panghu-planebattle-esm.git

将包导入到项目当中

第二步:提供一个空白的`<div>`用于渲染游戏

使用样例如下:
```javascript
<template>
  <div>
    <div ref="stage"></div>
  </div>
</template>

<script>
import { canvas, main_1 } from "panghu-planebattle-esm"
export default {
  mounted() {
    this.$refs.stage.appendChild(canvas);
    main_1.maingame();
  }
}
</script>

<style>
#stage {
  width: 480px;
  height: 650px;
  margin: 0 auto;
}
</style>

```

<br>

### 1.2.在react中使用

...
我不会react,等我学完了react再来更新


<br>
<br>

## 2.游戏说明

**基本操作:在小游戏中,使用鼠标操作英雄发射子弹击败敌机**
**当鼠标离开游戏画面后,游戏自动暂停**

**英雄:仅有三点生命值,每次与敌机碰撞后减少一点生命**

**敌机:共有三类**

|     敌机类型     |  生命值  | 击杀后得分 |
| :--------------: | :-----: | :--------: |
|   第一使徒(中)   |   10    |    1分    |
|   第二使徒(大)   |   50    |    5分    |
|  第一使徒(超大)  |   100   |    20分   |

**子弹射速:每秒发射8枚子弹**

**随机掉落奖励类:**
会随机掉落一个篮球
英雄拾取后,消灭全场敌机,但不得分


## 3.参数修改

该包所有文件的配置项参数都在config.js文件中

可修改参数包括:
1. 图片地址             在IMAGES对象中进行修改
2. 敌机血量             life属性 在E1,E2,E3中
3. 敌机大小             width和height属性 在E1,E2,E3中             
4. 敌机击杀后获得分数    score属性 在E1,E2,E3中
5. 敌机飞行速度最大值    maxSpeed属性 在E1,E2,E3中
6. 敌机飞行速度最小值    minSpeed属性 在E1,E2,E3中
7. 英雄生命              window.life
8. 分数                  window.score             

<br>
<br>

## 4.关于小游戏的更多(包括整个游戏的开发过程)
Html飞机大战(一):绘制动态背景
Html飞机大战(九): 使徒来袭 （设计敌机）
Html飞机大战(十七): 优化移动端
[label](https://i.cnblogs.com/posts?page%3D1%26cateId%3D2260336)


<br>
<br>

## 5.存在问题

这个小游戏目前还存在很多问题:
如:
1. 英雄和敌机的撞击判断存在很多问题

```javascript
//main.js中的全局方法:
function checkHit() {
            // 遍历所有的敌机
            for (let i = 0; i < enemies.length; i++) {
                //检测英雄是否撞到敌机
                if (enemies[i].hit(hero)) {
                    //将敌机和英雄的destory属性改为true
                    enemies[i].collide();
                    hero.collide();
                }
                
            }
        }
```
enemy对象中的hit方法
```javascript
hit(o) {
    let ol = o.x;
    let or = o.x + o.width;
    let ot = o.y;
    let ob = o.y + o.height;
    let el = this.x;
    let er = this.x + this.width;
    let et = this.y;
    let eb = this.y + this.height;
    if (ol > er || or < el || ot > eb || ob < et) {
      return false;
    } else {
      return true;
    }
  }
```

你应该看出来了,这是一个"正方形"判定,这会导致很多问题
例如,我的英雄还没有"真正"碰到敌机,
而是英雄的左上角与敌机的右下角接触
(想像两个正方形接触)

英雄的"身体"没有碰到敌机的"身体"
就爆炸了

这会导致后期游戏体验非常差


2. 这个游戏不够好玩
就如字面意思一样,这个游戏不够好玩


<br>
<br>

## 6.欢迎加入我的仓库,我们一起完善这个项目

如果你对网页小游戏制作感兴趣,并有一定的js基础

联系我:18676525317(微信)
       1072253914 (QQ)
我的博客:https://www.cnblogs.com/FatTiger4399/
